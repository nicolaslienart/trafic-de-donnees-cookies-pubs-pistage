console.log('Lancement de l\'extension de navigation browseTheWeb')

let sitesweb = [
	{
		name: 'Google',
		duration: 74888,
		links: [
			"https://www.google.com/search?source=hp&q=recette+pancakes&oq=recette+panca",
			"https://www.google.com/search?q=recette+pancakes&source=lnms&tbm=isch&sa=X&biw=1024&bih=526",
		]
	},
	{
		name: 'Orange',
		duration: 54900,
		links: [
			"https://boutique.orange.fr/internet-mobile/pack-open-fibre/open-up-70go",
			"https://boutique.orange.fr/mobile/samsung-galaxy-a71-noir?segmentforfait=FORFAIT_ILLIMITE",
		]
	},
	{
		name: 'Le Bon Coin',
		duration: 68416,
		links: [
			"https://www.leboncoin.fr/",
			"https://www.leboncoin.fr/chaussures/1822711774.htm/",
		]
	},
	{
		name: 'Cdiscount',
		duration: 106800,
		links: [
			"https://www.cdiscount.com/soldes-promotions/v-14107-14107.html#cm_sp=PA:SL:4_1:SlideR%20Parasol%20/%20Cave",
			"https://www.cdiscount.com/bricolage/climatisation/whirlpool-pacb29co-2500-watts-climatiseur-mobile/f-1661301-whi8003437237669.html",
		]
	},
	{
		name: 'Le Monde',
		duration: 261333,
		links: [
			"https://www.lemonde.fr/sciences/article/2020/07/30/la-nasa-a-lance-jeudi-son-astromobile-pour-chercher-des-traces-de-vie-passee-sur-mars_6047722_1650684.html",
		]
	},
]

let boucles
let index_site
let index_link
let tabid

let url;

async function start() {
	console.log("Starting", Date.now())
	boucles = 2
	index_site = 0
	index_link = 0
	url = sitesweb[index_site].links[index_link]
	tabid = await browser.tabs.query({currentWindow: true})
	update()
}

function switchUrlToNextLink() {
	index_link++

	if (index_link >= sitesweb[index_site].links.length) {
		index_site++

		if (index_site >= sitesweb.length) {
			boucles--
			index_site = 0
		}

		index_link = 0

	}

	url = sitesweb[index_site].links[index_link]
}

function update() {
	tabId = tabid.find(tab => tab.active === true).id
	if (boucles > 0) {
		browser.tabs.update(tabId, {
			url
		}).then(() => {
			setTimeout(update, sitesweb[index_site].duration)
			switchUrlToNextLink()
		})
	}
	else {
		console.log("Done", Date.now())
		browser.tabs.update(tabId, {
			url: 'about:blank'
		})
	}
}

chrome.browserAction.onClicked.addListener(start)
