# Méthode de mesure

Le trafic est mesuré directement via les outils de développement intégrés à Firefox (Devtools), spécifiquement avec l'onglet Réseau

## Configuration des Devtools

 * Option `Désactiver le cache` cochée
 * Option `Conserver les journaux` cochée (pour conserver les requêtes d'une page à l'autre)

## Récupération des logs

Une fois la navigation terminée on peut exporter les logs afin de les conserver et les consulter plus tard.
Il suffit de cliquer sur `Tout enregistrer en tant que HAR` dans le menu Réseau (roue crantée)

## Boucle

Afin que les données soit plus représentative la navigation se fait deux fois. Libre à l'utilisateur de modifier ce nombre de boucle, cela se fait dans le code de l'application (variable `boucles`).

## Facteurs d'influence

Il y a plusieurs facteurs d'influence des résultats dans ce test.
 * Rapidité de la connexion Internet qui peut limiter le trafic. Cependant la durée de la visite devrait réduire son impact sur le résultat.
 * Mise à jour des pages pendant le test. Une page peut voir son contenu être mis à jour avec l'ajout ou la suppression de contenu pendant la phase de navigation sur le domaine. Pour réduire cet impact on peut augmenter le nombre de boucles.
 * Mauvaise sélection de sites et pages. Il se peut que les pages visitées ne soient pas représentatives de l'impact global.
