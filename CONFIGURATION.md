# Configurations

## Navigateur

Mozilla Firefox dans la version Developer Edition 80.0beta
Sorti le 28 juillet 2020

## Comparaison

La mesure se fait sur une version témoin et une version avec les éléments bloqués

### Configuration spécifique de la version témoin

#### Paramètres du navigateur

`about:preferences` > `Vie privée` > `Protection renforcée contre le pistage` :
Configuré en mode personnalisé avec certaines protections du navigateur décochées :
 - Cookies
 - Contenu utilisé pour le pistage

Le "Do not track" est aussi désactivé car il peut influencer les sites

#### Extensions

Sans extension avec influence

### Configuration spécifique de la version avec blocage

#### Paramètres du navigateur

`about:preferences` > `Vie privée` > `Protection renforcée contre le pistage` :
Configuré en mode personnalisé avec certaines protections du navigateur cochées dont :
 - Cookies (tiers)
 - Contenu utilisé pour le pistage

L'envoie du "Do not track" est activé

#### Extensions

[uBlock Origin](https://addons.mozilla.org/en-US/android/addon/ublock-origin/), bloqueur de publicités et d'outils de pistage Open-Source et réputé efficace. En configuration par défaut.
Version 1.29.2

## Configuration avancée `about:config`
`browser.cache.disk.enable`        désactivé
`browser.cache.memory.enable`      désactivé
`dom.image-lazy-loading.enabled`   désactivé
