# Mesure de l'impact des cookies, publicités et du pistage sur le trafic de donnée sur Internet

## Usage

1. Configurer le navigateur (`CONFIGURATION.md`)
2. Charger l'extension, se rendre sur `about:debugging#/runtime/this-firefox` puis cliquer sur le bouton `Charger un module complémentaire temporaire...` et séléctionner un fichier du dossier `extension` de ce dépot GIT
3. Optionnel : cliquer sur `Examiner` pour consulter la console de l'extension et la déboguer
4. Ouvrir un nouvel onglet sur une page vierge `about:blank`
5. Ouvrir les devtools de cet onglet (F12)
6. Aller dans l'onglet `Réseau` et vérifier la configuration (voir `MESURE.md`)
7. Cliquer sur l'icône de l'extension (forme de pièce de puzzle verte) pour lancer la simulation
8. Laisser faire jusqu'à que l'onglet revienne sur une page vierge `about:blank`.
9. Télécharger les logs (voir `M̀ESURE.md`)

## Rapport *Écologie du Digital - État des lieux et démarches de sobriété*

Ce dispositif a permis d'obtenir des statistiques approximatifs pour le rapport *Écologie du Digital - État des lieux et démarches de sobriété* mené par Nicolas Liénart.

[Dépot du rapport mis à jour](https://framagit.org/nicolaslienart/rapport-eco-num-web)

## Objectif

Mesurer la différence de trafic de données de la navigation sur Internet entre l'usage des cookies, du pistage et de la publicité et lorsqu'ils sont bloqués.
Conclure de l'influence ou non de ces éléments sur la consommation de données.

## Dispositif

Il s'agit de simuler une navigation sur des sites web et collecter les flux HTTP au moyen du navigateur Firefox et d'une extension spécialement conçue pour gérer le passage de page en page.

Pour consulter les détails de la configuration se référer à `CONFIGURATION.md`

Pour consulter la méthode de mesure se référer à `MESURE.md`

### Méthode de navigation

J'ai développé une extension qui une fois activée va procéder à une simulation de navigation, l'URL de l'onglet est changée au bout d'un certain temps pour passer à la page suivante, pour comprendre comment cette durée est déterminée se référer à `SITES.md`. Une fois que la navigation est terminée, l'extension change l'URL sur `about:blank` pour interrompre complètement le trafic.

## Contributions

Ces éléments ont été mis en ligne à des fins de transparence, afin que la communauté puisse vérifier les chiffres et avancées ainsi que corriger des bugs ou adapter le moyen de mesure pour le rendre plus fiable.
Chacun est libre de contribuer, veillez à bien expliquer vos observations pour faciliter le dialogue svp.

## Auteur

Je suis Nicolas Liénart, étudiant dans le multimédia à l'école d'ingénieur IMAC (Univeristé Gustave Eiffel) et je mène actuellement ce projet de rapport de façon indépendante de toute société ou institution.
L'environnement et sa préservation me tiennent à coeur et je veux éviter que le numérique soit destructeur. Je veux donc aider les autres personnes qui font usage du numérique à identifier les problématiques écologiques et à adopter une démarche plus sobre et à mener leurs projets de façon respectueuse.
