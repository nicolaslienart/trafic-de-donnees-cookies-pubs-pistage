# Sélection des sites et pages

J'ai récupérer des sites parmi les plus consultés en France selon le [classement Alexa](https://www.alexa.com/topsites/countries/FR)
 * Google.fr
 * Orange.fr
 * LeBonCoin.fr
 * Cdiscount.com
 * LeMonde.fr

Puis j'ai sélectionné une à deux pages pour chaque domaine, avec soit la page d'accueil soit une page séléctionnée de manière arbitraire.

Toujours dans l'idée d'être représentatif, le temps à passer sur chaque page dépend du site. En effet j'ai récuppéré le temps moyen passé par les visiteurs sur les sites que j'ai divisé par le nombre de pages consultées par visite, les chiffres viennent de [SimilarWeb](https://www.similarweb.com/fr/) de juillet 2020.

## Pourquoi le temps de consultation est important

Aujourd'hui les sites sont très dynamiques, rarement une page web sera chargée d'une traite, des requêtes continueront d'être transmises afin de communiquer avec le serveur ou pour mettre à jour la page en continu sans nécessiter un rechargement.
